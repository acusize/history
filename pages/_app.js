import "../styles/tailwind.css";
import { storyblokInit, apiPlugin } from "@storyblok/react";
import Feature from "../components/Feature";
import Grid from "../components/Grid";
import Page from "../components/Program";
import Teaser from "../components/Teaser";
import Program from "../components/Program";
import Hero from "../components/Hero";

const components = {
  grid: Grid,
  teaser: Teaser,
  feature: Feature,
  posts: Program,
  Hero: Hero,
};

storyblokInit({
  accessToken: "Zdjrv1iNVrSVNHdBKiyrkwtt",
  use: [apiPlugin],
  components,
}); 

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
