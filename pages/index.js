import Layout from "../components/Layout";
import {
  useStoryblokState,
  getStoryblokApi,
  StoryblokComponent,
  storyblokEditable
} from "@storyblok/react";

export default function Home({ story }) {
  story = useStoryblokState(story, {}, true);

  if(story === false) return null
  return (
    <Layout>
      <StoryblokComponent blok={story.content} />
    </Layout>
  );
}

export async function getStaticProps(context) {
  // the slug of the story
  let slug = "malta";
  let params = {
    version: "published", // or 'published'
  };

  const storyblokApi = getStoryblokApi();

  let { data } = await storyblokApi.get(`cdn/stories/${slug}`, params);

  return {
    props: {
      story: data ? data.story : false,
      key: data ? data.story.id : false,
    },
  };
}
