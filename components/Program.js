import { storyblokEditable, StoryblokComponent } from "@storyblok/react";

const Program = ({ blok }) => {
  return (
    <main className="" {...storyblokEditable(blok)} key={blok._uid}>
      <h1>{blok.title}</h1>
      <p>{blok.subtitle}</p>
      <h1>EDIT</h1>
      {blok.body.map((nestedBlok) => (
        <StoryblokComponent blok={nestedBlok} key={nestedBlok._uid} />
      ))}
    </main>
  )
};


export default Program;
