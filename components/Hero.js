import { storyblokEditable, StoryblokComponent } from "@storyblok/react";

const Hero = ({ blok }) => {
    return (
        <div className="" {...storyblokEditable(blok)} key={blok._uid}>
           <img src={blok.bg.filename} alt='filename' />
           <div>{blok.description.content[0].content[0].text}</div>
        </div>
    )
};

export default Hero;